<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Vinyl;
use App\Form\VinylType;
use App\Repository\UserRepository;
use App\Repository\VinylRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminVinylController extends AbstractController
{

    /* HOME PAGE ADMIN */

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    /* USERS ADMIN */

    /**
     * @Route("/admin/users", name="admin_users")
     */
    public function home(UserRepository $repository): Response
    {
        $users = $repository->findAll();
        return $this->render('admin/admin_users.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/admin/users/{id}", name="admin_users_suppression", methods="delete")
     */
    public function suppressionUser(User $user, Request $request, EntityManagerInterface $manager) {
        if($this->isCsrfTokenValid('SUP' . $user->getId(), $request->get('_token'))) {
            $manager->remove($user);
            $manager->flush();
            return $this->redirectToRoute('admin_users');
        }
    }

    /* VINYLS ADMIN */

    /**
     * @Route("/admin/vinyls", name="admin_vinyls")
     */
    public function vinyl(VinylRepository $repository): Response
    {
        $vinyls = $repository->findAll();
        return $this->render('admin/admin_vinyls.html.twig', [
            'vinyls' => $vinyls,
        ]);
    }

    /**
     * @Route("/admin/vinyls/creation", name="admin_vinyls_creation")
     * @Route("/admin/vinyls/{id}", name="admin_vinyls_edition", methods="GET|POST")
     */
    public function creationEtEdition(Vinyl $vinyl = null, Request $request, EntityManagerInterface $manager)
    {
        if(!$vinyl) {
            $vinyl = new Vinyl();
        }
        $form = $this->createForm(VinylType::class, $vinyl);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($vinyl);
            $manager->flush();
            return $this->redirectToRoute('admin_vinyls');
        }
        return $this->render('admin/edition_vinyls.html.twig', [
            'vinyl' => $vinyl,
            'form' => $form->createView(),
            'modification' => $vinyl->getId() !== null,
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin_vinyls_suppression", methods="delete")
     */
    public function suppressionVinyl(Vinyl $vinyl, Request $request, EntityManagerInterface $manager) {
        if($this->isCsrfTokenValid('SUP' . $vinyl->getId(), $request->get('_token'))) {
            $manager->remove($vinyl);
            $manager->flush();
            return $this->redirectToRoute('admin_vinyls');
        }
    }
}


