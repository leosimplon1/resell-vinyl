<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    /**
     * @Route("/user/{id}", name="user")
     */
    public function index(User $user): Response
    {
        return $this->render('user/index.html.twig', [
        'user' => $user,
        ]);
    }

    /**
     * @Route("/user/modification/{id}", name="user_modification", methods="GET|POST")
     */
    public function creationEtEdition(User $user = null, Request $request, EntityManagerInterface $manager)
    {
        if(!$user) {
            $user = new User();
        }
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('user');
        }
        return $this->render('user/user_edition.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'modification' => $user->getId() !== null,
        ]);
    }
}
