<?php

namespace App\Controller;

use App\Entity\Vinyl;
use App\Form\VinylType;
use App\Repository\VinylRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class VinylController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        
        return $this->render('vinyl/home.html.twig');
    }

    /**
     * @Route("/vinyls", name="vinyls")
     */
    public function vinyl(VinylRepository $repository): Response
    {
        $vinyls = $repository->findAll();
        return $this->render('vinyl/vinyls.html.twig', [
        'vinyls' => $vinyls,
        ]);
    }
    
    /**
     * @Route("/sell", name="sell")
     */
public function creation(Vinyl $vinyl = null, Request $request, EntityManagerInterface $manager):Response
{
  if(!$vinyl) {
    $user = $this->getUser();
    $vinyl = new Vinyl();
  }
  $form = $this->createForm(VinylType::class, $vinyl);
  $form->handleRequest($request);
  if($form->isSubmitted() && $form->isValid()) {
    $vinyl->setVinylUser($user);
    $manager->persist($vinyl);
      $manager->flush();
      return $this->redirectToRoute('vinyls');
    }
    return $this->render('sell/sell.html.twig', [
      'vinyl' => $vinyl,
      'form' => $form->createView(),
      'username' => $user->getFirstname().' '.$user->getName(),
    ]);
  }
}





