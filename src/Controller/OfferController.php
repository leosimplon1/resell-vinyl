<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Form\OfferType;
use App\Repository\OfferRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OfferController extends AbstractController
{
    /**
     * @Route("/offers", name="offers")
     */
    public function index(OfferRepository $repository): Response
    {
        $offers = $repository->findAll();
        return $this->render('offer/offers.html.twig', [
            'offers' => $offers,
        ]);
    }

        /**
     * @Route("/add_offer", name="add_offer")
     */
    public function creation(Offer $offer = null, Request $request, EntityManagerInterface $manager):Response
    {
    if(!$offer) {
        $user = $this->getUser();
        $offer = new Offer();
        $offer->setPostby($user->getId());
    }
    $form = $this->createForm(OfferType::class, $offer);
    $form->handleRequest($request);
    if($form->isSubmitted() && $form->isValid()) {
        $manager->persist($offer);
        $manager->flush();
        return $this->redirectToRoute('offers');
    }
    return $this->render('offer/add_offer.html.twig', [
        'offers' => $offer,
        'form' => $form->createView(),
        'username' => $user->getFirstname().' '.$user->getName(),
        ]);
    }
}
