<?php

namespace App\Form;

use App\Entity\Vinyl;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VinylType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image')
            ->add('artist')
            ->add('album')
            ->add('year')
            ->add('genre', ChoiceType::class, [
                'choices' => [
                    '' => '',
                    'Rock' => 'rock',
                    'Pop' => 'Pop',
                    'Jazz' => 'Jazz',
                    'Soul' => 'Soul',
                    'Rap' => 'Rap',
                    'Folk' => 'Folk',
                    'Punk' => 'Punk',
                    'Metal' => 'Metal',
                    'Hip-hop' => 'Hip-hop',
                    'RnB' => 'Rnb',
                    'Blues' => 'Blues',
                    'Country' => 'Country',
                    'Funk' => 'Funk',
                    'Reggae' => 'Reggae',
                    'Electro' => 'Electro',
                ]
            ])
            ->add('price')
            ->add('vinyl_condition', ChoiceType::class, [
                'choices' => [
                    '' => '',
                    'Neuf' => 'Neuf',
                    'Excellent' => 'Excellent',
                    'Très bon état' => 'Très bon état',
                    'Bon état' => 'Bon état',
                    'Satisfaisant' => 'Satisfaisant',
                    'Usé' => 'Usé',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vinyl::class,
        ]);
    }
}
