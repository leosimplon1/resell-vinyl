<?php

namespace App\DataFixtures;

use App\Entity\Offer;
use App\Entity\User;
use App\Entity\Vinyl;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $a1 = new Vinyl();
        $a1->setArtist('Yung Lean')
            ->setAlbum('Starz')
            ->setYear(2020)
            ->setGenre('Hip-Hop')
            ->setPrice('25.00')
            ->setImage('images/yunglean.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a1);

        $a2 = new Vinyl();
        $a2->setArtist('Joy Division')
            ->setAlbum('Unknow Pleasures')
            ->setYear(1979)
            ->setGenre('Post Punk')
            ->setPrice('50.00')
            ->setImage('images/joydivision.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a2);

        $a3 = new Vinyl();
        $a3->setArtist('Daft Punk')
            ->setAlbum('Homework')
            ->setYear(1997)
            ->setGenre('French Touch')
            ->setPrice('130.00')
            ->setImage('images/daftpunk.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a3);

        $a4 = new Vinyl();
        $a4->setArtist('2pac')
            ->setAlbum('Me against the world')
            ->setYear(1995)
            ->setGenre('Hip-Hop')
            ->setPrice('40.00')
            ->setImage('images/2pac.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a4);

        $a5 = new Vinyl();
        $a5->setArtist('ASAP Rocky')
            ->setAlbum('Long. Live. A$AP')
            ->setYear(2013)
            ->setGenre('Hip-Hop')
            ->setPrice('30.00')
            ->setImage('images/asaprocky.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a5);

        $a6 = new Vinyl();
        $a6->setArtist('Playboi Carti')
            ->setAlbum('Whole Lotta Red')
            ->setYear(2020)
            ->setGenre('Hip-Hop')
            ->setPrice('80.00')
            ->setImage('images/playboicarti.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a6);

        $a7 = new Vinyl();
        $a7->setArtist('Boards Of Canada')
            ->setAlbum('The Campfire headphase')
            ->setYear(2005)
            ->setGenre('Musique électronique')
            ->setPrice('20.00')
            ->setImage('images/boardsofcanada.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a7);

        $a8 = new Vinyl();
        $a8->setArtist('Sébastien Tellier')
            ->setAlbum('Sexuality')
            ->setYear(2008)
            ->setGenre('Synthpop')
            ->setPrice('35.00')
            ->setImage('images/sebastientellier.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a8);

        $a9 = new Vinyl();
        $a9->setArtist('Kraftwerk')
            ->setAlbum('The Man-Machine')
            ->setYear(1978)
            ->setGenre('Musique électronique')
            ->setPrice('150.00')
            ->setImage('images/kraftwerk.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a9);

        $a10 = new Vinyl();
        $a10->setArtist('DJ Mehdi')
            ->setAlbum('Lucky Boy')
            ->setYear(2006)
            ->setGenre('Dance / électronique')
            ->setPrice('65.00')
            ->setImage('images/djmehdi.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a10);

        $a11 = new Vinyl();
        $a11->setArtist('Skepta')
            ->setAlbum('Ignorance Is Bliss')
            ->setYear(2019)
            ->setGenre('Hip-Hop')
            ->setPrice('72.00')
            ->setImage('images/skepta.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a11);

        $a12 = new Vinyl();
        $a12->setArtist('Tyler The Creator')
            ->setAlbum('Cherry Bomb')
            ->setYear(2015)
            ->setGenre('Hip-Hop')
            ->setPrice('32.00')
            ->setImage('images/tylerthecreator.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a12);

        $a13 = new Vinyl();
        $a13->setArtist('Travis Scott')
            ->setAlbum('Astroworld')
            ->setYear(2018)
            ->setGenre('Hip-Hop')
            ->setPrice('80.00')
            ->setImage('images/travisscott.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a13);

        $a14 = new Vinyl();
        $a14->setArtist('Kendrick Lamar')
            ->setAlbum('To Pimp a Butterfly')
            ->setYear(2015)
            ->setGenre('Hip-Hop')
            ->setPrice('60.00')
            ->setImage('images/kendricklamar.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a14);

        $a15 = new Vinyl();
        $a15->setArtist('Metronomy')
            ->setAlbum('The English Riviera')
            ->setYear(2011)
            ->setGenre('Pop')
            ->setPrice('20.00')
            ->setImage('images/metronomy.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a15);

        $a16 = new Vinyl();
        $a16->setArtist('Birdy Nam Nam')
            ->setAlbum('Manuel For Successful Rioting')
            ->setYear(2009)
            ->setGenre('Electronic Music')
            ->setPrice('32.00')
            ->setImage('images/birdynamnam.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a16);

        $a17 = new Vinyl();
        $a17->setArtist('The Chemical Brothers')
            ->setAlbum('Surrender')
            ->setYear(1999)
            ->setGenre('Electronic Music')
            ->setPrice('75.00')
            ->setImage('images/thechemicalbrothers.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a17);

        $a18 = new Vinyl();
        $a18->setArtist('Iam')
            ->setAlbum("L'école du micro d'argent")
            ->setYear(1997)
            ->setGenre('Hip-Hop')
            ->setPrice('45.00')
            ->setImage('images/iam.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a18);

        $a19 = new Vinyl();
        $a19->setArtist('Chief Keef')
            ->setAlbum('Finally Rich')
            ->setYear(2012)
            ->setGenre('Hip-Hop')
            ->setPrice('100.00')
            ->setImage('images/chiefkeef.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a19);

        $a20 = new Vinyl();
        $a20->setArtist('Massive Attack')
            ->setAlbum('Mezzanine')
            ->setYear(1998)
            ->setGenre('Electronic Music')
            ->setPrice('39.00')
            ->setImage('images/massiveattack.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a20);

        $a21 = new Vinyl();
        $a21->setArtist('Disclosure')
            ->setAlbum('Settle')
            ->setYear(2013)
            ->setGenre('Electronic Music')
            ->setPrice('10.00')
            ->setImage('images/disclosure.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a21);

        $a22 = new Vinyl();
        $a22->setArtist('Joey Bada$$')
            ->setAlbum('Before Da Money')
            ->setYear(2015)
            ->setGenre('Hip-Hop')
            ->setPrice('25.00')
            ->setImage('images/joeybadass.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a22);

        $a23 = new Vinyl();
        $a23->setArtist('Eminem')
            ->setAlbum('The Slim Shady LP')
            ->setYear(1999)
            ->setGenre('Hip-Hop')
            ->setPrice('79.00')
            ->setImage('images/eminem.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a23);

        $a24 = new Vinyl();
        $a24->setArtist('Michael Jackson')
            ->setAlbum('Off The Wall')
            ->setYear(1979)
            ->setGenre('Pop')
            ->setPrice('120.00')
            ->setImage('images/michaeljackson.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a24);

        $a25 = new Vinyl();
        $a25->setArtist('Oasis')
            ->setAlbum('Definitely Maybe')
            ->setYear(1994)
            ->setGenre('Rock')
            ->setPrice('60.00')
            ->setImage('images/oasis.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a25);

        $a26 = new Vinyl();
        $a26->setArtist('The Police')
            ->setAlbum('Ghost in the Machine')
            ->setYear(1981)
            ->setGenre('Rock')
            ->setPrice('49.00')
            ->setImage('images/thepolice.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a26);

        $a27 = new Vinyl();
        $a27->setArtist('David Bowie')
            ->setAlbum("Let's Dance")
            ->setYear(1983)
            ->setGenre('Rock')
            ->setPrice('150.00')
            ->setImage('images/davidbowie.jpg')
            ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a27);

        $a28 = new Vinyl();
        $a28->setArtist('The Clash')
            ->setAlbum('London Calling')
            ->setYear(1979)
            ->setGenre('Punk rock')
            ->setPrice('110.00')
            ->setImage('images/theclash.jpg')
                        ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a28);

        $a29 = new Vinyl();
        $a29->setArtist('Dr Dre')
            ->setAlbum('2001')
            ->setYear(1999)
            ->setGenre('Hip-Hop')
            ->setPrice('65.00')
            ->setImage('images/drdre.jpg')
                        ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a29);

        $a30 = new Vinyl();
        $a30->setArtist('Ramones')
            ->setAlbum('Ramones')
            ->setYear(1976)
            ->setGenre('Punk rock')
            ->setPrice('90.00')
            ->setImage('images/ramones.jpg')
                        ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a30);

        $a31 = new Vinyl();
        $a31->setArtist('Depeche Mode')
            ->setAlbum('Speak and Spell')
            ->setYear(1981)
            ->setGenre('Synthpop')
            ->setPrice('55.00')
            ->setImage('images/depechemode.jpg')
                        ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a31);

        $a32 = new Vinyl();
        $a32->setArtist('Prince')
            ->setAlbum('Purple Rain')
            ->setYear(1984)
            ->setGenre('Rock')
            ->setPrice('140.00')
            ->setImage('images/prince.jpg')
                        ->setPostby('leo.lecoufle')
            ->setVinylcondition('Bon');
        $manager->persist($a32);


        $b1 = new Offer();
        $b1->setDescription('Vinyle de Kendrick Lamar en promotion chez Cultura')
            ->setLink('www.cultura/kendrick-lamar-damn-vinyl.fr')
            ->setPostby('leo.lecoufle');
        $manager->persist($b1);

        $b2 = new Offer();
        $b2->setDescription('Vinyle de Serge Gainsbourg en promotion chez la Fnac')
            ->setLink('www.fnac/serge-gainsbourg-vinyl.fr')
            ->setPostby('daniel.dupont');
        $manager->persist($b2);

        $b3 = new Offer();
        $b3->setDescription('Platine vinyle Audio Technica AT-LP60XBK en promotion chez boulanger')
            ->setLink('https://www.boulanger.com/')
            ->setPostby('etienne.decrecy');
        $manager->persist($b3);

        $b4 = new Offer();
        $b4->setDescription('Vinyle de Charles Aznavour en promotion chez leclerc')
            ->setLink('www.leclerc/charles-aznavour-vinyl.fr')
            ->setPostby('paul.dupré');
        $manager->persist($b4);

        $b5 = new Offer();
        $b5->setDescription('Vinyle de Charles Aznavour en promotion chez leclerc')
            ->setLink('www.leclerc/charles-aznavour-vinyl.fr')
            ->setPostby('paul.dupré');
        $manager->persist($b5);

        $b6 = new Offer();
        $b6->setDescription('Vinyle de Charles Aznavour en promotion chez leclerc')
            ->setLink('www.leclerc/charles-aznavour-vinyl.fr')
            ->setPostby('paul.dupré');
        $manager->persist($b6);

        $b7 = new Offer();
        $b7->setDescription('Vinyle de Charles Aznavour en promotion chez leclerc')
            ->setLink('www.leclerc/charles-aznavour-vinyl.fr')
            ->setPostby('paul.dupré');
        $manager->persist($b7);

        $b8 = new Offer();
        $b8->setDescription('Vinyle de Charles Aznavour en promotion chez leclerc')
            ->setLink('www.leclerc/charles-aznavour-vinyl.fr')
            ->setPostby('paul.dupré');
        $manager->persist($b8);

        $b9 = new Offer();
        $b9->setDescription('Vinyle de Charles Aznavour en promotion chez leclerc')
            ->setLink('www.leclerc/charles-aznavour-vinyl.fr')
            ->setPostby('paul.dupré');
        $manager->persist($b9);


        $c1 = new User();
        $c1->setName('Admin')
            ->setFirstname('Admin')
            ->setEmail('admin@admin.fr')
            ->setPhone('0678785647')
            ->setPassword('azerty')
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($c1);

        $c2 = new User();
        $c2->setName('Bobby')
            ->setFirstname('Smurda')
            ->setEmail('bobby@bobby.fr')
            ->setPhone('0678983647')
            ->setPassword('azerty')
            ->setRoles(['ROLE_USER']);
        $manager->persist($c2);

        $c3 = new User();
        $c3->setName('Pierre')
            ->setFirstname('Lachamp')
            ->setEmail('pierre@pierre.fr')
            ->setPhone('0675283647')
            ->setPassword('azerty')
            ->setRoles(['ROLE_USER']);
        $manager->persist($c3);

        $c4 = new User();
        $c4->setName('Paul')
            ->setFirstname('Paule')
            ->setEmail('paul@paul.fr')
            ->setPhone('0678984647')
            ->setPassword('azerty')
            ->setRoles(['ROLE_SELLER']);
        $manager->persist($c4);

        $c5 = new User();
        $c5->setName('Jean')
            ->setFirstname('Dupré')
            ->setEmail('jean@jean.fr')
            ->setPhone('0698325657')
            ->setPassword('azerty')
            ->setRoles(['ROLE_SELLER']);
        $manager->persist($c5);
        
        $manager->flush();

    }
}

