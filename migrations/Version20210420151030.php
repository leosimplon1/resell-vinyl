<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210420151030 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE vinyl ADD vinyl_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vinyl ADD CONSTRAINT FK_E2E531DA4333F1F FOREIGN KEY (vinyl_user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_E2E531DA4333F1F ON vinyl (vinyl_user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_8D93D64986CC499D ON user');
        $this->addSql('ALTER TABLE vinyl DROP FOREIGN KEY FK_E2E531DA4333F1F');
        $this->addSql('DROP INDEX IDX_E2E531DA4333F1F ON vinyl');
        $this->addSql('ALTER TABLE vinyl DROP vinyl_user_id');
    }
}
